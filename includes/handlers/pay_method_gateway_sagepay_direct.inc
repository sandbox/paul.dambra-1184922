<?php

/**
 * @file
 */

 /*
	This class contains as properties all of the variables that SagePay accept
	in a VSP Direct POST. Each is named using the string expected by SagePay 
	in the POST data so that the class can pass itself to http_build_query
	in order to quickly build the urlencoded key=>value pairs required.
 */
 class SagePayVspDirectPost {
	var $System = "SIMULATION";
	var $VpsProtocol = "2.23";
	
	var $AbortURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorAbortTx";
	var $AuthoriseURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorAuthoriseTx";
    var $CancelURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorCancelTx";
    var $PurchaseURL="https://test.sagepay.com/simulator/VSPDirectGateway.asp";
    var $RefundURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorRefundTx";
    var $ReleaseURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorReleaseTx";
    var $RepeatURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorRepeatTx";
    var $VoidURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorVoidTx";
    var $ThreeDCallbackPage="https://test.sagepay.com/simulator/VSPDirectCallback.asp";
    var $PayPalCompletionURL="https://test.sagepay.com/simulator/paypalcomplete.asp";
			  
	//PAYMENT, DEFERRED or AUTHENTICATE only
	var $TxType = "PAYMENT";
	//vendor login name
	var $Vendor;
	//This must be unique for each transaction sent 
	//Max. 40 Alphanumeric Characters (A-Z, a-z, 0-9, -, _, ., {, })
	var $VendorTxCode;
	var $PartnerId = "";
	var $Amount;
	/* 
		Set this to indicate the currency in which you wish to trade. You will need a merchant number in this currency 
		this variable could be replaced when currency support in included in pay
		http://drupal.org/node/932666 
	*/
	var $Currency = "GBP";
	// Up to 100 chars of free format description
	var $Description;
	var $CardHolder;
	var $CardNumber;
	//The two dates are expected as MMYY
	var $StartDate;
	var $ExpiryDate;
	var $IssueNumber;
	var $CV2;
	//VISA, MC, DELTA, MAESTRO, UKE, AMEX, DC, JCB, LASER, PAYPAL
	var $CardType;
	//Billing fields are compulsory and must have a value
	var $BillingSurname;
	var $BillingFirstNames;
	var $BillingAddress1;
	var $BillingAddress2;
	var $BillingCity;
	var $BillingPostCode;
	var $BillingCountry;
	var $BillingState;
	var $BillingPhone;
	//Delivery fields are compulsory and must have a value
	var $DeliverySurname;
	var $DeliveryFirstNames;
	var $DeliveryAddress1;
	var $DeliveryAddress2;
	var $DeliveryCity;
	var $DeliveryPostCode;
	var $DeliveryCountry;
	var $DeliveryState;
	var $DeliveryPhone;
	
	var $CustomerEmail;
	var $Basket;
	//0 = not gift aid, 1 = gift aid
	var $GiftAidPayment = 0;
	
	/*
	0 = If AVS/CV2 enabled then check them. If rules apply, use rules (default).
	1 = Force AVS/CV2 checks even if not enabled for the account. If rules apply, use rules.
	2 = Force NO AVS/CV2 checks even if enabled on account.
	3 = Force AVS/CV2 checks even if not enabled for the account but DON�T apply any rules.
	*/
	var $ApplyAvsCv2 = 0;
	/*0 = If 3D-Secure checks are possible and rules allow, perform the checks and apply the authorisation rules (default).
	1 = Force 3D-Secure checks for this transaction only (if your account is 3D-enabled) and apply rules for authorisation.
	2 = Do not perform 3D-Secure checks for this transaction only and always authorise.
	3 = Force 3D-Secure checks for this transaction (if your account is 3D-enabled) but ALWAYS obtain an auth code, irrespective of rule base.
	*/
	var $Apply3DSecure = 0;
	/*
	E = Use the e-commerce merchant account (default).
	C = Use the continuous authority merchant account (if present).
	M = Use the mail order, telephone order account (if present).
	*/
	var $AccountType = "E";
	
	var $ClientIpAddress;
		
	//Paypal variables
	var $isPayPalExpress = false;
	var $PayPalCallbackUrl;
	var $BillingAgreement = 0;
	
	
	/*
		if constructed with a System value of LIVE or TEST
		then the SagePay urls are set to LIVE or TEST sagepay system settings
		any other value means they use their default values 
		which point to the simulator system
	*/
	function __construct($setSystem = "SIMULATION"){
		$System = strtoupper($setSystem);
		if ($System=="LIVE")
			{
			  $AbortURL="https://live.sagepay.com/gateway/service/abort.vsp";
			  $AuthoriseURL="https://live.sagepay.com/gateway/service/authorise.vsp";
			  $CancelURL="https://live.sagepay.com/gateway/service/cancel.vsp";
			  $PurchaseURL="https://live.sagepay.com/gateway/service/vspdirect-register.vsp";
			  $RefundURL="https://live.sagepay.com/gateway/service/refund.vsp";
			  $ReleaseURL="https://live.sagepay.com/gateway/service/release.vsp";
			  $RepeatURL="https://live.sagepay.com/gateway/service/repeat.vsp";
			  $VoidURL="https://live.sagepay.com/gateway/service/void.vsp";
			  $ThreeDCallbackPage="https://live.sagepay.com/gateway/service/direct3dcallback.vsp";
			  $PayPalCompletionURL="https://live.sagepay.com/gateway/service/complete.vsp";
			}
			elseif ($System=="TEST")
			{
			  $AbortURL="https://test.sagepay.com/gateway/service/abort.vsp";
			  $AuthoriseURL="https://test.sagepay.com/gateway/service/authorise.vsp";
			  $CancelURL="https://test.sagepay.com/gateway/service/cancel.vsp";
			  $PurchaseURL="https://test.sagepay.com/gateway/service/vspdirect-register.vsp";
			  $RefundURL="https://test.sagepay.com/gateway/service/refund.vsp";
			  $ReleaseURL="https://test.sagepay.com/gateway/service/release.vsp";
			  $RepeatURL="https://test.sagepay.com/gateway/service/repeat.vsp";
			  $VoidURL="https://test.sagepay.com/gateway/service/void.vsp";
			  $ThreeDCallbackPage="https://test.sagepay.com/gateway/service/direct3dcallback.vsp";
			  $PayPalCompletionURL="https://test.sagepay.com/gateway/service/complete.vsp";
			}
			//properties are initialised with simulation urls
	}
	
	/*
		construct the SagePay post in urlEncoded key=>value pairs
		TODO depending on values can we just include blank unecessary fields??
	*/
	function ToPost(){
		//generate VendorTxCode
				$timeStamp = date("y/m/d : H:i:s", time());
		$randNum = rand(0,32000);
		$thisTxCode = $this->VendorTxCode."-".$timeStamp."-".$randNum;
		
		$postFields = array(
			'VpsProtocol'=>$this->VpsProtocol,
			'TxType'=>$this->TxType,
			'Vendor'=>$this->Vendor,
			'VendorTxCode'=>$this->thisTxCode,
			'Amount'=>$this->Amount,
			'Currency'=>$this->Currency,
			'Description'=>$this->Description,
			'CardHolder'=>$this->CardHolder,
			'CardNumber'=>$this->CardNumber,
			'StartDate'=>$this->StartDate,
			'ExpiryDate'=>$this->ExpiryDate,
			'IssueNumber'=>$this->IssueNumber,
			'CV2'=>$this->CV2,
			'CardType'=>$this->CardType,
			'BillingSurname'=>$this->BillingSurname,
			'BillingFirstNames'=>$this->BillingFirstNames,
			'BillingAddress1'=>$this->BillingAddress1,
			'BillingAddress2'=>$this->BillingAddress2,
			'BillingCity'=>$this->BillingCity,
			'BillingPostCode'=>$this->BillingPostCode,
			'BillingCountry'=>$this->BillingCountry,
			'BillingState'=>$this->BillingState,
			'BillingPhone'=>$this->BillingPhone,
			'DeliverySurname'=>$this->DeliverySurname,
			'DeliveryFirstNames'=>$this->DeliveryFirstNames,
			'DeliveryAddress1'=>$this->DeliveryAddress1,
			'DeliveryAddress2'=>$this->DeliveryAddress2,
			'DeliveryCity'=>$this->DeliveryCity,
			'DeliveryPostCode'=>$this->DeliveryPostCode,
			'DeliveryCountry'=>$this->DeliveryCountry,
			'DeliveryState'=>$this->DeliveryState,
			'DeliveryPhone'=>$this->DeliveryPhone,
			'CustomerEmail'=>$this->CustomerEmail,
			'Basket'=>$this->Basket,
			'GiftAidPayment'=>$this->GiftAidPayment,
			'ApplyAvsCv2'=>$this->ApplyAvsCv2,
			'Apply3DSecure'=>$this->Apply3DSecure,
			'AccountType'=>$this->AccountType,
			'ClientIpAddress'=>$this->ClientIpAddress,
			'PayPalCallbackUrl'=>$this->PayPalCallbackUrl,
			'BillingAgreement'=>$this->BillingAgreement
		);

		return http_build_query($postFields,'','&');
	}
 }
 
 
class pay_method_gateway_sagepay_direct extends pay_method_gateway {
	//pass "LIVE" or "TEST" into this constructor to move off the simulator
	var $SagePayRequest;
	//settings form variables
	var $sagepay_vendorname;
    var $sagepay_vendor_tx_code_prefix;
    var $sagepay_system;
    var $sagepay_currency;
    var $sagepay_tx_type;

	function __construct($values=NULL){
	    parent::__construct($values);
	    $this->SagePayRequest = new SagePayVspDirectPost($this->sagepay_system);
	}
	
	/* 
	This function is pretty self-explanatory. 
	TODO: This needs to return different URLs depending on which type of SagePay transaction is being carried out. For now this is just bog standard purchase
	*/
	function gateway_url() {
		return $this->SagePayRequest->PurchaseURL;
	}

	function AddInfoToSagePayRequest() {
	     $this->SagePayRequest->Vendor = $this->sagepay_vendorname;
	     $this->SagePayRequest->VendorTxCode = $this->sagepay_vendor_tx_code_prefix;
         $this->SagePayRequest->Currency = $this->sagepay_currency;
         $this->SagePayRequest->TxType = $this->sagepay_tx_type;
		 $this->SagePayRequest->CardType = $this->cc_type;
		 $this->SagePayRequest->CardNumber = $this->cc_number;
		 $this->SagePayRequest->CV2 = $this->cc_ccv2;
		 $this->SagePayRequest->ExpiryDate = $this->cc_expiration();
		 $this->SagePayRequest->IssueNumber = $this->cc_issue_number;
		 //TODO pay_method_gateway.inc doesn't seem to include a start date
		 dpm($this->billto,'billing address');
		 dpm($this->mail,'mail');
		 $this->SagePayRequest->BillingFirstNames = $this->first_name;
		 $this->SagePayRequest->BillingSurnames = $this->last_name;
	}
	
	/**
	* Create request to be POST to the gateway.
	*/
	function gateway_request() {
		//right now we should have cc info set so...
		$this->AddInfoToSagePayRequest();
		return $this->SagePayRequest->ToPost();
	}
 
  /**
   *
   */
  function gateway_response($result) {
	dpm($result,'response received');
	$status = $result->status_message;
	$statusDetail = $result->data;
	
	if($status =="3DAUTH"){
		//TODO - we need to redirect for 3d-secure authentication
	}
	elseif($status=="PPREDIRECT"){
		//TODO 
		/* The customer needs to be redirected to a PayPal URL as PayPal was chosen as a card type or
            ** payment method and PayPal is active for your account. A VPSTxId and a PayPalRedirectURL are
            ** returned in this response so store the VPSTxId in your database now to match to the response
            ** after the customer is redirected to the PayPalRedirectURL to go through PayPal authentication 
		*/
	}
	else{
		/* If this isn't 3D-Auth, then this is an authorisation result (either successful or otherwise) **
			** Get the results form the POST if they are there */
		$VPSTxId=$result["VPSTxId"];
		$SecurityKey=$result["SecurityKey"];
		$TxAuthNo=$result["TxAuthNo"];
		$AVSCV2=$result["AVSCV2"];
		$AddressResult=$result["AddressResult"];
		$PostCodeResult=$result["PostCodeResult"];
		$CV2Result=$result["CV2Result"];
		$ThreeDSecureStatus=$result["3DSecureStatus"];
		$CAVV=$result["CAVV"];
	}
	
	// Set Message Value
	if ($status=="OK")
		$messageValue="AUTHORISED - The transaction was successfully authorised with the bank.";
	elseif ($status=="MALFORMED")
		$messageValue="MALFORMED - The StatusDetail was:" . $statusDetail;
	elseif ($status=="INVALID")
		$messageValue="INVALID - The StatusDetail was:" . $statusDetail;
	elseif ($status=="NOTAUTHED")
		$messageValue="DECLINED - The transaction was not authorised by the bank.";
	elseif ($status=="REJECTED")
		$messageValue="REJECTED - The transaction was failed at 3D-Secure or AVS/CV2.";
	elseif ($status=="AUTHENTICATED")
		$messageValue="AUTHENTICATED - The transaction was successfully 3D-Secure Authenticated and can now be Authorised.";
	elseif ($status=="REGISTERED")
		$messageValue="REGISTERED - The transaction was could not be 3D-Secure Authenticated, but has been registered to be Authorised.";
	elseif ($status=="ERROR")
		$messageValue="ERROR - There was an error during the payment process.  The error details are: " . $statusDetail;
	else
		$messageValue="UNKNOWN - An unknown status was returned from Sage Pay.  The Status was: " . $status . ", with StatusDetail:" . $statusDetail;

	if (($status=="OK")||($status=="AUTHENTICATED")||($status=="REGISTERED"))
		//$transaction = $xml->Payment->TxnList->Txn;
		//$this->activity->identifier = (string)$transaction->txnID;
		return TRUE;
	else {
		watchdog('payment', $messageValue);
		drupal_set_message($messageValue);
		return FALSE;
	}
  }

	/**
	* Process a payment.
	*/
	function execute($activity) {
		dpm($activity);
		$this->activity = $activity;
		if ($request = $this->gateway_request()) {
		  $response = drupal_http_request(
							$this->gateway_url(), 
							0, 
							'POST', 
							$request);
		  if ($response->error) {
			watchdog('payment', "Gateway Error: @err Payment NOT processed.", array('@err' => $ret->error));
			$this->activity->data = (array) $ret;
			return FALSE;
		  }
		  else {
			return $this->gateway_response($response);
		  }
		}
	}

  /**
   * @return
   *   Array of HTTP headers in key => value pairs.
   * SagePay Direct wants no headers...
   */
  function gateway_headers() {
    return array();
    //return array('Content-Type' => 'application/x-www-form-urlencoded');
  }

  function settings_form(&$form, &$form_state) {
    parent::settings_form($form, $form_state);
    $group = $this->handler();
    
    
    $form[$group]['sagepay_direct']['#type'] = 'fieldset';
    $form[$group]['sagepay_direct']['#collapsible'] = FALSE;
    $form[$group]['sagepay_direct']['#title'] = t('SagePay Direct Payment API Plug-in settings');
    $form[$group]['sagepay_direct']['#group'] = $group;

    $form[$group]['sagepay_direct']['sagepay_vendorname'] = array(
      '#type' => 'textfield',
      '#title' => t('SagePay VendorName'),
      '#default_value' => $this->sagepay_vendorname,
      '#required' => TRUE,
      '#parents' => array($group, 'sagepay_vendorname'),
    );
    $form[$group]['sagepay_direct']['sagepay_vendor_tx_code_prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Transaction Code Prefix'),
      '#default_value' => $this->sagepay_vendor_tx_code_prefix,
      '#required' => TRUE,
      '#parents' => array($group, 'sagepay_vendor_tx_code_prefix'),
    );
    $form[$group]['sagepay_direct']['sagepay_system'] = array(
      '#type' => 'radios',
	  '#options' => array('TEST'=> 'TEST', 'SIMULATION' => 'SIMULATION', 'LIVE' => 'LIVE'),
      '#title' => t('SagePay Payment Systems'),
      '#description' => t('Which of the three SagePay systems should the payment be sent to'),
      '#default_value' => $this->sagepay_system,
      '#parents' => array($group, 'sagepay_system'),
    );
    $form[$group]['sagepay_direct']['sagepay_currency'] = array(
      '#type' => 'textfield',
	  '#title' => t('Three Letter Currency Code'),
	  '#description' => t('E.g GBP Currently we\'re only able to set a single currency but SagePay accounts can be associated with more than one.'),
      '#default_value' => $this->sagepay_currency,
      '#parents' => array($group, 'sagepay_currency'),
    );
    $form[$group]['sagepay_direct']['sagepay_tx_type'] = array(
      '#type' => 'radios',
	  '#options' => array('PAYMENT'=> 'PAYMENT', 'AUTHENTICATE' => 'AUTHENTICATE', 'DEFERRED' => 'DEFERRED'),
      '#title' => t('Choose a payment type'),
      '#default_value' => $this->sagepay_tx_type,
      '#parents' => array($group, 'sagepay_tx_type'),
    );
  }
  
/**
   * Your payment method should return an array of valid currencies, using
   * 3-digit currency codes.  Example:
   *
   * return array('USD', 'EUR');
   * 
   * SagePay accepts any currency that you have a merchant banking account for.
   * full ist can be found via http://www.iso.org/iso/support/currency_codes_list-1.htm
   * TODO jiggery pokey at some point to allow multiple currencies in this module or multiple instances of this module
   */
  function available_currencies() {
    return array('GBP','EUR');
  }
}
